#include <inputcontainer.h>


InputContainer::InputContainer()
{
    QHBoxLayout *lo = new QHBoxLayout();
    this->setLayout(lo);
    QFrame *frame = new QFrame();
    this->layout()->addWidget(frame);
    frame->setStyleSheet(
        "background-color: #52489c;"
        "border: 1px solid black;"
        "border-radius: 10px;"
    );

    vbox = new QVBoxLayout();
    frame->setLayout(vbox);

    button = new QPushButton();
    titleInput = new QLineEdit;
    descriptionInput = new QLineEdit;

    button->setText("Agregar");
    titleInput->setPlaceholderText("Título");
    titleInput->setFocus();
    titleInput->setMaxLength(40);
    descriptionInput->setPlaceholderText("Descripción");

    hbox = new QHBoxLayout();
    vbox->addLayout(hbox, 1);
    hbox->addWidget(titleInput);
    hbox->setStretch(0, 1);
    hbox->setStretch(1, 1);
    hbox->addWidget(button);
    vbox->addWidget(descriptionInput);

    button->setStyleSheet(
        "background-color: #84a98c;"
        "padding: 2px;"
        "margin: 2px;"
        "border-radius: 2px;"
        "height: 30px;"
        "min-width: 150px;"
    );
    titleInput->setStyleSheet(
        "background-color: #59c3c3;"
        "padding: 2px;"
        "margin: 2px;"
        "border-radius: 2px;"
        "height: 30px;"
    );
    descriptionInput->setStyleSheet(
        "background-color: #59c3c3;"
        "padding: 2px;"
        "margin: 2px;"
        "border-radius: 2px;"
        "height: 30px;"
    );

}

InputContainer::~InputContainer(){

}
