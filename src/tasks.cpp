#include <tasks.hpp>
#include <cassert>
#include <math.h>
#include <iostream>

Id TaskContainer::nextId = 0;
const QString TaskContainer::databasePath = QString("state.json");

TaskContainer::TaskContainer()
    : todoList(QString("TODO"), TaskState::TODO),
      doingList(QString("DOING"), TaskState::DOING),
      doneList(QString("DONE"), TaskState::DONE)
{
    todoList.linkNext(&doingList);

    doingList.linkPrev(&todoList);
    doingList.linkNext(&doneList);

    doneList.linkPrev(&doingList);

    this->setStyleSheet("background-color: red");
    hbox = new QHBoxLayout();
    this->setLayout(hbox);
    hbox->addWidget(&todoList);
    hbox->addWidget(&doingList);
    hbox->addWidget(&doneList);

    loadState();
}
TaskContainer::~TaskContainer()
{
    saveState();
}
void TaskContainer::addTask(const QString &title,
                            const QString &description)
{
    Id newId = getId();

    std::cout << "Nuevo Id: " << newId << std::endl;

    Task *newTask = new Task(
        newId,
        title,
        description,
        TaskState::TODO,
        &todoList);
    todoList.addTask(newId, newTask);
}
Id TaskContainer::getId()
{

    //Implementación temporal...
    nextId++;
    return (nextId - 1);
}
// ---------- LOAD STATE ---------------
void TaskContainer::loadState()
{
    qDebug() << "Loading Tasks From Database";
    if (!QFileInfo::exists(databasePath))
        return;
    QJsonDocument doc;
    QJsonParseError err;
    {
        QFile fin(databasePath);
        fin.open(QIODevice::ReadOnly);
        QByteArray ba = fin.readAll();
        doc = QJsonDocument::fromJson(ba, &err);
    }

    if (err.error != QJsonParseError::NoError)
    {
        qWarning() << "ADVERTENCIA: Base de datos corrupta en "
                   << err.offset << ": " << err.errorString();
    }

    loadList(todoList, doc);
    loadList(doingList, doc);
    loadList(doneList, doc);

    QJsonObject metaInfo = doc.object()["META"].toObject();
    TaskContainer::nextId = (Id)metaInfo["lastId"].toString().toInt();
    qDebug() << "Next id:" << nextId;
    qDebug() << "Tasks Loaded";
}
void TaskContainer::loadList(TaskList& list, QJsonDocument& doc)
{
    qDebug() << "Loading State of List" << list.getName();
    QJsonObject listState = doc.object()[list.getName()].toObject();
    QJsonArray tasks_array = listState["tasks"].toArray();
    for (int taskIdx = 0; taskIdx < tasks_array.size(); ++taskIdx) {
        QJsonObject task = tasks_array[taskIdx].toObject();
        Id id = (Id)task["id"].toString().toInt();
                qDebug() << "Loading id:" << id;
        Task *newTask = new Task(
            id,
            task["title"].toString(),
            task["description"].toString(),
            list.getState(),
            &list
        );
        list.addTask(id, newTask);
    }
    qDebug() << list.getName() << "Loaded";
}
void TaskContainer::saveState()
{
    QJsonObject database;
    qDebug() << "Saving to database";

    // ---- META INFO ------
    QJsonObject meta;
    meta["lastId"] = QString::number(nextId);

    // ---- LISTS ----
    QJsonObject todo, doing, done;
    todo = todoList.save();
    doing = doingList.save();
    done = doneList.save();

    // ---- FILL DATABASE ----
    database["META"] = meta;
    database[todoList.getName()] = todo;
    database[doingList.getName()] = doing;
    database[doneList.getName()] = done;

    // ---- SAVE ----
    QByteArray ba = QJsonDocument(database).toJson();
    {
        QFile fout(databasePath);
        fout.open(QIODevice::ReadWrite | QIODevice::Truncate | QIODevice::Text);
        fout.write(ba);
    }
    qDebug() << "Done saving";
}

TaskList::TaskList(const QString &name)
    : name(name), prev(nullptr), next(nullptr)
{
    this->setStyleSheet("background-color: blue");
    vbox = new QVBoxLayout();
    this->setLayout(vbox);
}
TaskList::TaskList(const QString &name, TaskState state)
    : name(name), state(state), prev(nullptr), next(nullptr)
{
    this->setStyleSheet("background-color: #ebebeb");
    vbox = new QVBoxLayout();
    this->setLayout(vbox);
}
TaskList::~TaskList()
{
    for (auto &task : tasks)
    {
        delete task.second;
    }
    for (auto &task : trash)
    {
        delete task.second;
    }
}
void TaskList::addTask(Id taskId, Task *task)
{
    assert(tasks.find(taskId) == tasks.end());

    tasks.insert(make_pair(taskId, task));
    task->setParent(this);
    updateView();
}
void TaskList::updgradeTask(Id taskId)
{
    assert(tasks.find(taskId) != tasks.end());
    if (next != nullptr)
    {
        tasks.at(taskId)->setState(next->getState());
        next->addTask(taskId, tasks.at(taskId));
    }
    else
    {
        trash.insert(make_pair(taskId, tasks.at(taskId)));
        tasks.at(taskId)->setParent(nullptr);
    }
    tasks.erase(taskId);
    updateView();

    for (auto &&item : tasks)
        cout << item.first << ": " << item.second << '\n';
}
void TaskList::downgradeTask(Id taskId)
{
    assert(tasks.find(taskId) != tasks.end());
    if (prev != nullptr)
    {
        tasks.at(taskId)->setState(prev->getState());
        prev->addTask(taskId, tasks.at(taskId));
        tasks.erase(taskId);
    }
    updateView();

    for (auto &&item : tasks)
        cout << item.first << ": " << item.second << '\n';
}
void TaskList::deleteTask(Id taskId)
{
    cout << "flag" << endl;
    cout << vbox->count() << endl;

    vbox->removeWidget(tasks[taskId]);

    delete tasks[taskId];
    tasks.erase(taskId);

    updateView();
}
QJsonObject& TaskList::save()
{
    QJsonObject* list = new QJsonObject();
    QJsonArray tasks_array;

    for (const auto& task : tasks) {
        QJsonObject temp;
        temp["id"] = QString::number(task.second->getId());
        temp["title"] = task.second->getTitle();
        temp["description"] = task.second->getDescription();
        tasks_array.push_back(temp);
    }
    (*list)["tasks"] = tasks_array;
    return *list;
}

// TODO IMPLEMENTAR

void TaskList::updateView()
{
    cout << "Update view" << endl;

    for (int i = 0; i < (int)vbox->count(); i++)
    {
        vbox->removeWidget(vbox->itemAt(i)->widget());
    }

    for (const auto &p : tasks)
        this->layout()->addWidget(p.second);
}

Task::Task(Id taskId,
           const QString &title,
           const QString &description,
           TaskState state,
           TaskList *parent)
    : parent(parent), title(title), description(description),
      id(taskId), state(state)
{

    QHBoxLayout *lo = new QHBoxLayout();
    this->setLayout(lo);
    QFrame *frame = new QFrame();
    this->layout()->addWidget(frame);

    hbox = new QHBoxLayout();
    frame->setLayout(hbox);
    titleLabel = new QLabel(title);
    titleLabel->setFixedHeight(25);
    hbox->addWidget(titleLabel);

    downgradeBtn = new QPushButton();
    downgradeBtn->setText("<");
    downgradeBtn->setEnabled(false);
    upgradeBtn = new QPushButton();
    upgradeBtn->setText(">");

    deleteBtn = new QPushButton();
    deleteBtn->setText("DEL");

    hbox->addWidget(downgradeBtn);
    hbox->addWidget(upgradeBtn);
    hbox->addWidget(deleteBtn);

    this->setToolTip(description);
    frame->setStyleSheet(
        "background-color: #ebebeb;"
    );

    frame->setStyleSheet(
        "background-color: #52489c;"
        "border: 1px solid black;"
        "border-radius: 10px;"
        "max-height: 50px;"
    );
    downgradeBtn->setStyleSheet(
        "background-color: #84a98c;"
        "padding: 2px;"
        "margin: 2px;"
        "border-radius: 2px;"
        "height: 15px;"
        "max-width: 30px;"
    );
    upgradeBtn->setStyleSheet(
        "background-color: #84a98c;"
        "padding: 2px;"
        "margin: 2px;"
        "border-radius: 2px;"
        "height: 15px;"
        "max-width: 30px;"
    );
    deleteBtn->setStyleSheet(
        "background-color: red;"
        "padding: 2px;"
        "margin: 2px;"
        "border-radius: 2px;"
        "height: 15px;"
        "max-width: 30px;"
    );
    titleLabel->setStyleSheet(
        "border: none"
    );
    titleLabel->setAutoFillBackground(true);
    downgradeBtn->setAutoFillBackground(false);

    connect(downgradeBtn, SIGNAL(pressed()), this, SLOT(onDowngrade()));
    connect(upgradeBtn, SIGNAL(pressed()), this, SLOT(onUpgrade()));
    connect(deleteBtn, SIGNAL(pressed()), this, SLOT(onDelete()));
}
Task::~Task()
{
}
void Task::setState(TaskState newState)
{
    state = newState;
}
void Task::setParent(TaskList *newParent)
{
    parent = newParent;
    if (state == TaskState::TODO)
    {
        downgradeBtn->setEnabled(false);
        upgradeBtn->setEnabled(true);
        return;
    }
    if (state == TaskState::DOING)
    {
        downgradeBtn->setEnabled(true);
        upgradeBtn->setEnabled(true);
        return;
    }
    if (state == TaskState::DONE)
    {
        downgradeBtn->setEnabled(true);
        upgradeBtn->setEnabled(false);
        return;
    }
}
// TODO IMPLEMENTAR
void Task::updateView()
{
    //    for(int i=0; i<(int)hbox->count(); i++){
    //        delete hbox->itemAt(i)->widget();
    //        hbox->removeWidget(hbox->itemAt(i)->widget());
    //    }
    //    hbox->addWidget(titleLabel);
    //    if (state == TaskState::TODO) {
    //        hbox->addWidget(upgradeBtn);
    //        hbox->addWidget(deleteBtn);
    //        return;
    //    }
    //    if (state == TaskState::DOING) {
    //        hbox->addWidget(downgradeBtn);
    //        hbox->addWidget(upgradeBtn);
    //        hbox->addWidget(deleteBtn);
    //        return;
    //    }
    //    if (state == TaskState::DOING) {
    //        hbox->addWidget(downgradeBtn);
    //        hbox->addWidget(deleteBtn);
    //        return;
    //    }
}

// TASK SIGNALS
void Task::onUpgrade()
{
    assert(parent != nullptr);
    parent->updgradeTask(id);
    updateView();
}
void Task::onDowngrade()
{
    assert(parent != nullptr);
    parent->downgradeTask(id);
    updateView();
}

void Task::onDelete()
{
    assert(parent != nullptr);
    parent->deleteTask(id);
}
