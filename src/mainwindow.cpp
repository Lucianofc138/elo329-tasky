#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <iostream>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    this->setStyleSheet("background-color: #ebebeb;");
    centralWidget = new QWidget();
    this->setCentralWidget(centralWidget);
    this->setFixedWidth(800);
    vbox = new QVBoxLayout();
    centralWidget->setLayout(vbox);

    inputContainer = new InputContainer();
    vbox->insertWidget(0, inputContainer);

    //titulos
    titulos = new Titles();
    vbox->insertWidget(1,titulos);

    taskContainer = new TaskContainer();
    vbox->insertWidget(2, taskContainer);

    vbox->setStretch(0, 1);
    vbox->setStretch(2, 3);

    createActions();
}

MainWindow::~MainWindow()
{
    delete ui;
    delete inputContainer;
}

void MainWindow::createActions(){
    connect(inputContainer->button, SIGNAL(pressed()), this, SLOT(onAddButton()));
}

void MainWindow::onAddButton(){
    std::cout << "Click" << std::endl;
    std::cout << inputContainer->getTitle().toStdString() << endl;
    std::cout << inputContainer->getDescription().toStdString() << endl;
    if (inputContainer->getTitle()=="" || inputContainer->getDescription()=="")
        return;
    taskContainer->addTask(
                inputContainer->getTitle(),
                inputContainer->getDescription());
}

Titles::Titles(){
    hbox = new QHBoxLayout();
    this->setLayout(hbox);

    all->setAlignment(Qt::AlignCenter);doing->setAlignment(Qt::AlignCenter);done->setAlignment(Qt::AlignCenter);
    all->setStyleSheet("QLabel { background-color : #CAD2C5; color : black; border: 1px solid black;}");
    doing->setStyleSheet("QLabel { background-color : #CAD2C5; color : black; border: 1px solid black;}");
    done->setStyleSheet("QLabel { background-color : #CAD2C5; color : black; border: 1px solid black;}");

    hbox->addWidget(all);
    hbox->addWidget(doing);
    hbox->addWidget(done);
}

Titles::~Titles(){
    delete hbox;
    delete all;delete doing;delete done;
}
