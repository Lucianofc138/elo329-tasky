#ifndef INPUTCONTAINER_H
#define INPUTCONTAINER_H

#include <QObject>
#include <QWidget>
#include <string>
#include <QLineEdit>
#include <QPushButton>
#include <QGridLayout>
#include <QFrame>
#include <QBoxLayout>
#include <QVBoxLayout>
#include <QHBoxLayout>

using namespace std;

class InputContainer: public QWidget
{
    Q_OBJECT
public:
    InputContainer();
    ~InputContainer();
    QString getTitle() const { return titleInput->text();};
    QString getDescription() const { return descriptionInput->text();};
    QPushButton *button;

private:
    QLineEdit *titleInput;
    QLineEdit *descriptionInput;
    QVBoxLayout *vbox;
    QHBoxLayout *hbox;
    QFrame *frame;
};

#endif // INPUTCONTAINER_H
