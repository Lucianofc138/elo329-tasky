#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QWidget>
#include <QVBoxLayout>
#include "inputcontainer.h"
#include "tasks.hpp"

class MainWindow;
class Titles;

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

public slots:
    void onAddButton();

private:
    Ui::MainWindow *ui;
    void createActions();
    InputContainer *inputContainer;
    TaskContainer *taskContainer;
    QWidget *centralWidget;
    QVBoxLayout *vbox;
    Titles *titulos;
};

class Titles : public QWidget{

    Q_OBJECT
public:
    Titles();
    ~Titles();
private:
    QHBoxLayout *hbox;
    QLabel *all = new QLabel("Por hacer"),
           *doing = new QLabel("En progreso"),
           *done = new QLabel("Hecho");
};

#endif // MAINWINDOW_H
