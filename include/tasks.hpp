#ifndef TASKS_H
#define TASKS_H

#include <map>
#include <QObject>
#include <QWidget>
#include <string>
#include <QLineEdit>
#include <QPushButton>
#include <QGridLayout>
#include <QLayout>
#include <QHBoxLayout>
#include <QFrame>
#include <QLabel>
#include <QJsonObject>
#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonParseError>
#include <QFile>
#include <QFileInfo>
#include <QDebug>

class Task;
class TaskList;
class TaskContainer;
using namespace std;
using TaskMap = map<int, Task *>;
using Block = uint32_t;
using Id = uint32_t;
const Block FULL_BLOCK = 0xFFFFFFFF;
const Block EMPTY_BLOCK = 0x00000000;
const size_t BLOCK_SIZE = sizeof(uint32_t);

enum TaskState
{
    TODO,
    DOING,
    DONE
};

/* Task ----------------------------------------------------------
 *
 *
-------------------------------------------------------------------*/

class Task : public QWidget
{
    Q_OBJECT
public:
    Task(Id taskId,
         const QString &title,
         const QString &description,
         TaskState state = TaskState::TODO,
         TaskList *parent = nullptr);
    ~Task();
    void setState(TaskState newState);
    void setParent(TaskList *newParent);
    // TODO IMPLEMENTAR UPDATE VIEW
    void updateView();
    Id getId() const { return id; };
    QString getTitle() const { return title; }
    QString getDescription() const { return description; }
public slots:
    void onUpgrade();
    void onDowngrade();
    void onDelete();

private:
    TaskList *parent;
    QString title, description;
    Id id;
    TaskState state;
    // TODO AGREGAR CLASES QT PARA MANEJAR VISTA
    QFrame *frame;
    QHBoxLayout *hbox;
    QLabel *titleLabel;
    QPushButton *upgradeBtn;
    QPushButton *downgradeBtn;
    QPushButton *deleteBtn;
};

/* TaskList ------------------------------------------------------
 *
 *
-------------------------------------------------------------------*/

class TaskList : public QWidget
{
    Q_OBJECT
public:
    TaskList(const QString &name);
    TaskList(const QString &name, TaskState state);
    ~TaskList();
    void linkPrev(TaskList *link) { prev = link; }
    void linkNext(TaskList *link) { next = link; }
    void addTask(Id taskId, Task *task);
    void updgradeTask(Id taskId);
    void downgradeTask(Id taskId);
    void deleteTask(Id taskId);
    void updateView();

    QString getName() const { return name; }
    TaskState getState() const { return state; }

    QJsonObject& save();

private:
    QString name;
    TaskState state;
    TaskMap tasks;
    TaskMap trash;
    TaskList *prev, *next;
    // TODO AGREGAR CLASES QT PARA MANEJAR VISTA
    QVBoxLayout *vbox;
};

/* TaskContainer ---------------------------------------------------
 *
 *
-------------------------------------------------------------------*/

class TaskContainer : public QWidget
{
    Q_OBJECT
public:
    TaskContainer();
    //TaskContainer(json);
    ~TaskContainer();
    void addTask(const QString &title,
                 const QString &description);
    void releaseId(Id id);

private:
    // Metodos para manipular IDs
    Id getId();
    static Id nextId;

    std::vector<Block> idBag;
    TaskList todoList, doingList, doneList;
    // TODO AGREGAR CLASES QT
    // TODO PARA MANEJAR VISTA DE TASKLIST

    QHBoxLayout *hbox;

    // JSON
    static const QString databasePath;
    void loadState();
    void saveState();
    void loadList(TaskList &list, QJsonDocument &doc);
};

#endif // TASKS_H
