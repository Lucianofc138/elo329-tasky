# Tasky

Proyecto que consiste en una aplicación para crear lista de
tareas.

# Instalación

Dentro de la carpeta raiz, ejecutar los siguientes comandos.

```
$ mkdir -p build && cd build
$ qmake ..
$ make
```

Luego para ejecutar:

```
$ ./Proyecto
```

Alternativamente se puede compilar y ejecutar desde QtCreator.
